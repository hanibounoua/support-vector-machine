import numpy as np
import cupy as cp



class SVMc:
    def __init__(self, Lambda = .5, C = 1, kernel = 'linear'):

        # Model's Hyper-Parameters:
        self.Lambda = Lambda # Ponderation Parameter for on the  Hinge Loss function
        self.C = C # The Cost Parameter of the mis classified points on the Hinge Loss function.
        self.kernel = kernel # Not linear transformation of D
        

        # Model's Parameters:
        self.omega = None

    def fit(self, X, y, lr = .001, n_iter = 1000):
        n_sample, n_feature = X.shape
        xp = cp.get_array_module(X)
        if xp.__name__ == "cupy":
            print(xp.__name__)
            y = xp.asarray(y)
        new_col = xp.ones(shape=(n_sample,1))
        X = xp.hstack((X, new_col))
        y = xp.where(y == 0, -1, 1) # Here I ve used the same nomencalture as I found.
        # On tutorial, So this variable is used to define positive and negative class.
        self.omega = xp.zeros(n_feature + 1)
        for i in range(n_iter):
            z = xp.where(y * (self.omega.dot(X.transpose())) >= 1, 0, y)
            penalisation = self.C * z.dot(X) / n_sample
            self.omega -= lr * (self.Lambda * self.omega - penalisation)

    def predict(self, X):
        xp = cp.get_array_module(X)
        n_sample, _ = X.shape
        new_col = xp.ones(shape=(n_sample,1))
        X = xp.hstack((X, new_col))
        y = xp.where(np.sign(self.omega.dot(X.transpose())) > 0, 1, 0)
        return y
