import numpy as np
import cupy as cp
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, confusion_matrix
import matplotlib.pyplot as plt

from SVM import SVMc

import time


def run_cpu():
    X, y =  datasets.make_blobs(n_samples=1000000, n_features=2, centers = 2, cluster_std = 1.05, random_state = 123)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = .33, random_state=123)
    model = SVMc()
    model.fit(X_train, y_train, n_iter=1000)
    y_predicted = model.predict(X_test)
    AUC = roc_auc_score(y_test, y_predicted)
    con_mat = confusion_matrix(y_test, y_predicted)
    return {"AUC": AUC, "Confusion Matrix": con_mat}

def run_gpu():
    X, y =  datasets.make_blobs(n_samples=1000000, n_features=2, centers = 2, cluster_std = 1.05, random_state = 123)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = .33, random_state=123)
    X_train = cp.asarray(X_train)
    y_train = cp.asarray(y_train)
    X_test = cp.asarray(X_test)
    y_test = cp.asarray(y_test)
    model = SVMc()
    model.fit(X_train, y_train, n_iter=1000)
    y_predicted = model.predict(X_test)
    AUC = roc_auc_score(cp.asnumpy(y_test), cp.asnumpy(y_predicted))
    con_mat = confusion_matrix(cp.asnumpy(y_test), cp.asnumpy(y_predicted))
    return {"AUC": AUC, "Confusion Matrix": con_mat}

if __name__ == "__main__":
    start_cpu = time.time()
    result_cpu = run_cpu()
    end_cpu = time.time()
    h_delim = "="*50
    print(f"CPU : {h_delim}\nAUC : {result_cpu['AUC']}\nConfusion Matrix : \n{result_cpu['Confusion Matrix']}\nGPU Exec time : {end_cpu - start_cpu}")

    start_gpu = time.time()
    result_gpu = run_gpu()
    end_gpu = time.time()
    h_delim = "="*50
    print(f"GPU : {h_delim}\nAUC : {result_gpu['AUC']}\nConfusion Matrix : \n{result_gpu['Confusion Matrix']}\nCPU Exec time : {end_gpu - start_gpu}")